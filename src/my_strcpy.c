/*
** my_strcpy.c for my_strcpy.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Jan 17 19:10:22 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 17 19:11:30 2016 Raidouane EL MOUKHTARI
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}
