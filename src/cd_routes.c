/*
** cd_routes.c for  in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 02:02:17 2016 Raidouane EL MOUKHTARI
** Last update Fri Jan 22 14:31:30 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	change_pwdvar(char ***env, char *path)
{
  int	i;
  int	stop;

  stop = 0;
  i = 0;
  while ((*env)[i] != NULL && stop == 0)
    {
      if (my_strncmp((*env)[i], "PWD", 3) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    {
      if (my_strstr(path, "..", 2) == 0)
        {
	  if (realloc_a_var(env, i, path) == -1)
	    return (-1);
	}
      else if (my_strstr(path, "..", 2) == 1 &&
	       (my_strstr(find_path(*env, "PWD"), "PWD=", 4)) == 0)
        (*env)[i] = back_pwd((*env)[i]);
      return (1);
    }
  return (-1);
}

int	check_if_direcory_exist(DIR *dir, char **newargv, char ***env, int pass)
{
  if (dir)
    {
      chdir(newargv[1]);
      if (pass == 0)
        if (change_pwdvar(env, newargv[1]) == -1)
	  return (-1);
      return (1);
    }
  else
    {
      my_putstr("bash: cd: ");
      my_putstr(newargv[1]);
      my_putstr(": Aucun fichier ou dossier de ce type\n");
      return (-1);
    }
}

int	do_cd(char **newargv, char ***env)
{
  DIR	*directory;
  int	pass;

  pass = 0;
  if (newargv[1] == NULL)
    {
      if (do_cd_basic(env) == 1)
        return (1);
      else
        return (-1);
    }
  if (my_strstr(newargv[1], "-", 1) == 1)
    {
      if (do_back_cd(env) == -1)
	return (-1);
      newargv[1] = parse_var_env(recover_env_var(*env, "PWD"));
      pass = 1;
    }
  directory = opendir(newargv[1]);
  return (check_if_direcory_exist(directory, newargv, env, pass));
}
