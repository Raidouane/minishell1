/*
** my_exec.c for my_exec.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Jan  5 14:22:24 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 17:15:07 2016 Raidouane EL MOUKHTARI
*/

#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include "my.h"

int	lauch_child_process(char *program, char **newargv, char **env)
{
  if (access(newargv[0], F_OK) == 0)
    {
      execve(newargv[0], newargv, env);
      return (1);
    }
  else
    {
      my_putstr(program);
      my_putstr(": Command not found.\n");
      exit(1);
    }
}

int	my_exec(char *program, char **newargv, char **env)
{
  pid_t	child_pid;
  int	status;

  child_pid = fork();
  if (child_pid < 0)
    {
      my_putstr("error: failed to fork\n");
      exit (-1);
    }
  else if (child_pid == 0)
    return (lauch_child_process(program, newargv, env));
  else if (child_pid > 0)
    waitpid(child_pid, &status, WUNTRACED);
  if (WIFSIGNALED(status))
    {
      if (WTERMSIG(status) == SIGSEGV)
	my_putstr("Segmentation fault\n");
    }
  return (-1);
}

int	free_newargv(char **newargv, char *path)
{
  int	i;

  i = 0;
  if (my_strstr(newargv[0], path, my_strlen(path)) == 0)
    free(path);
  while (newargv[i] != NULL)
    {
      free(newargv[i]);
      i++;
    }
  return (0);
}

int	launch(char *s, char *path, int nb_space, char ***env)
{
  char	**newargv;
  int	nb_args;

  newargv = NULL;
  nb_args = 0;
  nb_args = parse_my_args(s, path, nb_space);
  newargv = my_newargs(s, path, nb_args, *env);
  if (my_strstr(path, "exit", 4) == 1)
    {
      my_putstr("exit\n");
      exit(EXIT_SUCCESS);
    }
  else if (my_strstr(path, "setenv", 6) == 1)
    my_setenv(env, newargv, nb_args);
  else if (my_strstr(path, "unsetenv", 8) == 1)
    my_unsetenv(env, newargv, nb_args);
  else if (my_strstr(path, "cd", 2) == 1)
    do_cd(newargv, env);
  else if (my_strstr(path, "cd", 2) == 0 || my_strstr(path, "setenv", 6) == 0 ||
	   my_strstr(path, "unsetenv", 8) == 0)
    my_exec(path, newargv, *env);
  return (free_newargv(newargv, path));
}

int	main_loop(char ***env)
{
  char		*path;
  int		nb_space;
  char		*s;

  while (42)
    {
      my_putstr("$> ");
      signal(SIGINT, sigintHandler);
      nb_space = 0;
      s = get_next_line(0);
      if (s == NULL)
	{
	  my_putstr("exit\n");
	  exit(0);
	}
      check_first_av(path = parse_my_path(s, &nb_space));
      if (check_path(path) == 1)
	{
	  my_putstr(path);
	  my_putstr(": Command not found.\n");
	}
      else if ((check_first_av(path = parse_my_path(s, &nb_space))) != 1)
	launch(s, path, nb_space, env);
      free(s);
    }
}

