/*
** parsing_of_env.c for parsing_of_env.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Jan 17 19:26:59 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 17 19:27:59 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*find_path(char **env, char *wanted)
{
  int	i;
  int	stop;

  stop = 0;
  i = 0;
  while (env[i] != NULL && stop == 0)
    {
      if (my_strncmp(env[i], wanted, 4) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    return (env[i]);
  else
    return (NULL);
}
