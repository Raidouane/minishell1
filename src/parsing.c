/*
** parsing.c for parsing.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 01:03:23 2016 Raidouane EL MOUKHTARI
** Last update Fri Jan 22 01:32:46 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*parse_my_path_next(char *s, int i, int size)
{
  char	*path;

  if ((path = malloc(size + 1)) == NULL)
    return (NULL);
  size = 0;
  while (s[i] != ' ' && s[i] != '\t' && s[i] != '\0')
    {
      path[size] = s[i];
      i++;
      size++;
    }
  path[size] = '\0';
  return (path);
}

char	*parse_my_path(char *s, int *space)
{
  int	i;
  int	size;
  char	*path;
  int	save_i;

  i = 0;
  size = 0;
  path = NULL;
  while ((s[i] == ' ' || s[i] == '\t') && s[i] != '\0')
    {
      i++;
      (*space)++;
    }
  save_i = i;
  while (s[i] != ' ' && s[i] != '\t' && s[i] != '\0')
    {
      i++;
      size++;
    }
  i = save_i;
  if ((path = parse_my_path_next(s, i, size)) == NULL)
    return (NULL);
  return (path);
}

int	parse_my_args(char *s, char *path, int nb_space)
{
  int	i;
  int	jump;
  int	cancel;

  cancel = 0;
  i = 0;
  jump = my_strlen(path) + nb_space;
  while (s[jump] != '\0')
    {
      if (s[jump] == ' ' || s[jump] == '\t')
        {
          i++;
          while ((s[jump] == ' ' || s[jump] == '\t') && s[jump] != '\0' && s)
            jump++;
        }
      if (s[jump] == '\0')
        cancel = 1;
      if (s[jump] != '\0' && s)
        jump++;
    }
  if (cancel == 1)
    i--;
  return (i);
}

char	*parse_uservar(char *uservar)
{
  int	i;
  int	size;
  char	*user_name;

  size = 0;
  i = 0;
  if (uservar == NULL)
    return (NULL);
  while (uservar[size] != '\0' && uservar[size] != '=')
    size++;
  if (uservar[size] == '\0')
    return (NULL);
  size++;
  if ((user_name = malloc(size)) == NULL)
    return (NULL);
  while (uservar[size] != '\0' && uservar[size] != ' ')
    {
      user_name[i] = uservar[size];
      i++;
      size++;
    }
  user_name[i] = '\0';
  return (user_name);
}

char	*path_home_coll(char *user)
{
  char	*path_home;

  if ((path_home = malloc(6)) == NULL || user == NULL)
    return (NULL);
  path_home[0] = '/';
  path_home[1] = 'h';
  path_home[2] = 'o';
  path_home[3] = 'm';
  path_home[4] = 'e';
  path_home[5] = '/';
  path_home[6] = '\0';
  my_strcat(path_home, user);
  free(user);
  return (path_home);
}
