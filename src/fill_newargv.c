/*
** fill_newargv.c for fill_newargv.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 00:24:28 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 00:11:14 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*parse_my_cmd_next(char *s, int i, int *nb_space, int *nb_tab)
{
  char	*str;
  int	size;

  size = 0;
  str = NULL;
  while ((s[i] == ' ' || s[i] == '\t') && s[i] != '\0')
    {
      if (s[i] == '\t')
	(*nb_tab)++;
      if (s[i] == ' ')
	(*nb_space)++;
      i++;
    }
  while (s[i] != '\0' && s[i] != ' ' && s[i] != '\t')
    {
      i++;
      size++;
    }
  if ((str = malloc(size + 1)) == NULL)
    return (NULL);
  return (str);
}

char	*fill_str_parse_my_cmd(char *str, int i, char *s)
{
  int	size;

  size = 0;
  while ((s[i] == ' ' || s[i] == '\t') && s[i] != '\0')
    i++;
  while (s[i] != '\0' && s[i] != ' ' && s[i] != '\t')
    {
      str[size] = s[i];
      i++;
      size++;
    }
  str[size] = '\0';
  return (str);
}

char	*parse_my_cmd(char *s, char *already_read, int *nb_space, int *nb_tab)
{
  int	i;
  char	*str;
  int	jump;

  jump = 0;
  i = 0;
  str = NULL;
  while ((s[jump] == ' ' || s[jump] == '\t') && s[jump] != '\0')
    {
      if (s[jump] == '\t')
        (*nb_tab)++;
      if (s[jump] == ' ')
        (*nb_space)++;
      jump++;
    }
  i = my_strlen(already_read) + jump;
  if ((str = parse_my_cmd_next(s, i, nb_space, nb_tab)) == NULL)
    return (NULL);
  return (fill_str_parse_my_cmd(str, i, s));
}

char	*fill_newargv_next(char *save, char **read, int nb_space, int nb_tab)
{
  int	t;

  t = 0;
  while ((*read)[t] != '\0')
    {
      save[t] = (*read)[t];
      t++;
    }
  while (nb_space > 0)
    {
      save[t] = ' ';
      t++;
      nb_space--;
    }
  while (nb_tab > 0)
    {
      save[t] = '\t';
      t++;
      nb_tab--;
    }
  save[t] = '\0';
  free(*read);
  if ((*read = malloc(t)) == NULL)
    return (NULL);
  return (save);
}

char	**fill_newargv(char *s, char **read, char **newargv, int i)
{
  int	size;
  char	*save;
  char	*str_parsed;
  int	nb_space;
  int	nb_tab;

  nb_space = 0;
  nb_tab = 0;
  str_parsed = parse_my_cmd(s, *read, &nb_space, &nb_tab);
  size = my_strlen(str_parsed);
  if ((newargv[i] = malloc(size + 1)) == NULL)
    return (NULL);
  newargv[i] = my_strcpy(newargv[i], str_parsed);
  free(str_parsed);
  if ((save = malloc(my_strlen(*read) +
		     nb_space + nb_tab + my_strlen(newargv[i]) + 100)) == NULL)
    return (NULL);
  if ((save = fill_newargv_next(save, read, nb_space, nb_tab)) == NULL)
    return (NULL);
  if (concaitain_read(read, save, newargv[i]) == NULL)
    return (NULL);
  return (newargv);
}
