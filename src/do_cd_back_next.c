/*
** do_cd_back_next.c for do_cd_back_next.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 02:50:00 2016 Raidouane EL MOUKHTARI
** Last update Fri Jan 22 02:50:25 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*create_oldpwd_var(char **env)
{
  char	*oldpwd;
  char	*home_var;

  home_var = parse_var_env(recover_env_var(env, "HOME"));
  if ((oldpwd = malloc(sizeof(char *) * 8 + my_strlen(home_var))) == NULL)
    return (NULL);
  oldpwd[0] = 'O';
  oldpwd[1] = 'L';
  oldpwd[2] = 'D';
  oldpwd[3] = 'P';
  oldpwd[4] = 'W';
  oldpwd[5] = 'D';
  oldpwd[6] = '=';
  oldpwd[7] = '\0';
  oldpwd = my_strcat(oldpwd, home_var);
  return (oldpwd);
}
