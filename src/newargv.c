/*
** newargv.c for newargv.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 01:01:40 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 13:56:21 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*concaitain_read(char **read, char *save, char *str)
{
  free(*read);
  if ((*read = malloc(my_strlen(save) + my_strlen(str) + 2)) == NULL)
    return (NULL);
  *read = my_strcpy(*read, save);
  free(save);
  *read = my_strcat(*read, str);
  return (*read);
}

char	*fill_possibility(char *path_env, int jump, char *cmd, int save_jump)
{
  char	*str;
  int	t;

  t = 0;
  str = NULL;
  while (path_env[jump] != ':' && path_env[jump] != '\0')
    jump++;
  if ((str = malloc((jump - save_jump) + 2 + my_strlen(cmd))) == NULL)
    return (NULL);
  jump = save_jump;
  while (path_env[jump] != ':' && path_env[jump] != '\0')
    str[t++] = path_env[jump++];
  str[t] = '/';
  str[t + 1] = '\0';
  return (str);
}

char	*check_cmd(char *path_env, char *cmd, char *cpy_cmd, int jump)
{
  char	*str;
  int	save_jump;

  if (cpy_cmd == NULL && access(cmd, F_OK) == 0)
    return (cmd);
  if (cpy_cmd != NULL)
    {
      if (access(cpy_cmd, F_OK) == 0)
        return (cpy_cmd);
      else
        cpy_cmd = NULL;
    }
  if (jump >= my_strlen(path_env))
    return (NULL);
  save_jump = jump;
  if ((str = fill_possibility(path_env, jump, cmd, save_jump)) == NULL)
    return (NULL);
  jump = save_jump + my_strlen(str);
  if ((cpy_cmd = malloc(my_strlen(cmd) + my_strlen(str) + 3)) == NULL)
    return (NULL);
  cpy_cmd = my_strcpy(cpy_cmd, str);
  free(str);
  cpy_cmd = my_strcat(cpy_cmd, cmd);
  return (check_cmd(path_env, cmd, cpy_cmd, jump));
}

char	**my_newargs(char *s, char *path, int nb_args, char **env)
{
  int	i;
  char	**newargv;
  char	*read;
  char	*newpath;

  i = 1;
  read = NULL;
  if ((read = malloc(my_strlen(path) * sizeof(char *) + 3)) == NULL)
    return (NULL);
  read = my_strcpy(read, path);
  newpath = check_cmd(find_path(env, "PATH"), path, NULL, 5);
  if ((newargv = malloc(sizeof(char **) * (nb_args + 2))) == NULL)
    return (NULL);
  while (i <= nb_args)
    {
      newargv = fill_newargv(s, &read, newargv, i);
      i++;
    }
  newargv[i] = NULL;
  if (newpath == NULL)
    newargv[0] = path;
  else
    newargv[0] = newpath;
  free(read);
  return (newargv);
}
