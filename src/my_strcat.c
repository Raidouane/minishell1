/*
** my_strcat.c for my_strcat.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Jan 11 16:06:09 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 17 19:12:37 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*my_strcat(char *dest, char *src)
{
  int	ct;
  int	lg;

  ct = 0;
  lg = 0;
  while (dest[lg] != '\0' && dest)
    lg++;
  while (src[ct] != '\0' && src)
    {
      dest[lg] = src[ct];
      lg++;
      ct++;
    }
  dest[lg] = '\0';
  return (dest);
}
