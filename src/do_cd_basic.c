/*
** do_cd_basic.c for do_cd_basc.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 02:14:56 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 01:45:16 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*write_pwd(char *var)
{
  var[0] = 'P';
  var[1] = 'W';
  var[2] = 'D';
  var[3] = '=';
  return (var);
}

int	change_pwd_only_cd(char ***env)
{
  int	stop;
  int	i;
  char	*home_var;

  i = 0;
  stop = 0;
  while ((*env)[i] != NULL && stop == 0)
    {
      if (my_strncmp((*env)[i], "PWD", 6) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    {
      home_var = parse_var_env(recover_env_var(*env, "HOME"));
      if (((*env)[i] = malloc(my_strlen(home_var) + 6)) == NULL)
        return (-1);
      write_pwd((*env)[i]);
      (*env)[i] = my_strcat((*env)[i], home_var);
    }
  return (1);
}

int	do_cd_basic(char ***env)
{
  char	*home;

  if ((home = path_home_coll(parse_uservar(find_path(*env, "USER"))))
      == NULL)
    return (-1);
  chdir(home);
  if (change_pwd_only_cd(env) == -1)
    return (-1);
  return (1);
}
