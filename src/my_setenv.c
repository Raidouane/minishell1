/*
** my_setenv.c for my_setenv.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 14:03:38 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 13:35:54 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	edit_var(char ***env, char **newargv, int nb_args)
{
  char	*new;
  int	i;
  int	stop;

  stop = 0;
  i = 0;
  new = create_new_var(newargv, nb_args);
  while ((*env)[i] != NULL && stop == 0)
    {
      if (my_strncmp((*env)[i], newargv[1], my_strlen(newargv[1])) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    (*env)[i] = new;
  else
    return (-1);
  return (1);
}

int	realloc_env_setenv(char ***env, char **newargv, int nb_args)
{
  int	size;
  char	**new_env;
  char	*new_var;
  char	*find_var_in_env;

  size = 0;
  find_var_in_env = search_var(newargv[1], *env);
  if (find_var_in_env == NULL)
    {
      while ((*env)[size] != NULL)
        size++;
      if ((new_env = malloc(sizeof(char **) * (size + 2))) == NULL)
        return (-1);
      new_env = fill_env_to_new_env(env, new_env);
      new_var = create_new_var(newargv, nb_args);
      if ((new_env[size] = malloc(my_strlen(new_var) + 1)) == NULL)
        return (-1);
      new_env[size] = my_strcpy(new_env[size], new_var);
      new_env[size + 1] = NULL;
      *env = new_env;
      return (1);
    }
  else
    edit_var(env, newargv, nb_args);
  return (1);
}

int	my_setenv(char ***env, char **newargv, int nb_args)
{
  int	i;

  i = 0;
  if (nb_args == 0)
    {
      while (*env && (*env)[i] != NULL)
        {
          my_putstr((*env)[i]);
          my_putchar('\n');
          i++;
        }
      return (0);
    }
  if (nb_args > 2)
    {
      my_putstr("setenv: Too many arguments.\n");
      return (-1);
    }
  realloc_env_setenv(env, newargv, nb_args);
  return (1);
}
