/*
** main.c for main.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 14:18:49 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 14:05:04 2016 Raidouane EL MOUKHTARI
*/

#include <signal.h>
#include "my.h"

int	check_first_av(char *path)
{
  if (my_strlen(path) == 0)
    return (1);
  else
    return (0);
}

int	check_path(char *path)
{
  int	pass;

  pass = 0;
  if (path[0] == '.'  && access(path, F_OK) != 0)
    pass = 1;
  if (path[0] == '/'  && access(path, F_OK) != 0)
    pass = 1;
  return (pass);
}

void	sigintHandler()
{
  my_putstr("\n$> ");
}

int	 main(int ac, char **av, char **env)
{
  if (ac != 1)
    return (-1);
  main_loop(&env);
  if (av[1] != NULL)
      return (-1);
  return (0);
}
