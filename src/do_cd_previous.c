/*
** do_cd_previous.c for do_cd_previous.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 01:57:55 2016 Raidouane EL MOUKHTARI
** Last update Fri Jan 22 03:39:53 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*back_pwd_next(int i, char *pwdvar, int nb_slash)
{
  char	*str;
  int	count;

  if ((str = malloc(i + 1)) == NULL)
    return (NULL);
  i = 0;
  count = 0;
  while (i <= my_strlen(pwdvar) && count < nb_slash)
    {
      if (pwdvar[i] == '/')
        count++;
      str[i] = pwdvar[i];
      i++;
    }
  if (str[i - 1] == '/')
    str[i - 1] = '\0';
  else
    str[i] = '\0';
  return (str);
}

char	*back_pwd(char *pwdvar)
{
  char	*str;
  int	i;
  int	count;
  int	save_count;

  i = 0;
  count = 0;
  while (i <= my_strlen(pwdvar))
    {
      if (pwdvar[i] == '/')
        count++;
      i++;
    }
  i = 0;
  save_count = count;
  count = 0;
  while (i <= my_strlen(pwdvar) && count < save_count)
    {
      if (pwdvar[i] == '/')
        count++;
      i++;
    }
  if ((str = back_pwd_next(i, pwdvar, save_count)) == NULL)
    return (NULL);
  return (str);
}
