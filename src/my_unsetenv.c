/*
** my_unsetenv.c for my_unsetenv.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 14:09:51 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 02:51:37 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	parse_var_unsetenv(char *s1, char *s2)
{
  int	size;
  char	*str;

  size = 0;
  while (s1 && s1[size] != '\0' && s1[size] != '=')
    size++;
  if (s1[size] == '=')
    {
      if ((str = malloc(size + 1)) == NULL)
	return (-1);
      size = 0;
      while (s1 && s1[size] != '\0' && s1[size] != '=')
	{
	  str[size] = s1[size];
	  size++;
	}
      str[size] = '\0';
      if (my_strstr(str, s2, my_strlen(s2)) == 1)
	return (1);
    }
  return (-1);
}

char	**fill_env_to_new_env_unsetenv(char ***env, char **new_env,
				       char *var_to_delete)
{
  int	i;
  int	l;

  i = 0;
  l = 0;
  while (*env && (*env)[i] != NULL)
    {
      if (parse_var_unsetenv((*env)[i], var_to_delete) == 1)
        i++;
      new_env[l] = (*env)[i];
      if ((*env)[i] != NULL)
	i++;
      l++;

    }
  new_env[l] = NULL;
  return (new_env);
}

int	realloc_env_unsetenv(char ***env, char *var_to_delete)
{
  int	size;
  char	**new_env;
  char	*find_var_in_env;

  size = 0;
  find_var_in_env = recover_env_var(*env, var_to_delete);
  if (find_var_in_env != NULL)
    {
      while ((*env)[size] != NULL)
        size++;
      if ((new_env = malloc(sizeof(char **) * (size + 1))) == NULL)
        return (-1);
      new_env = fill_env_to_new_env_unsetenv(env, new_env, var_to_delete);
      *env = new_env;
      return (1);
    }
  return (0);
}

int	my_unsetenv(char ***env, char **newargv, int nb_args)
{
  if (nb_args > 0)
    realloc_env_unsetenv(env, newargv[1]);
  else if (nb_args == 0)
    {
      my_putstr("unsetenv: Too few arguments.\n");
      return (-1);
    }
  return (1);
}
