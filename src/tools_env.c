/*
** tools_env.c for tools_env.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 02:39:31 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 01:59:04 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*parse_var_env(char *var)
{
  int	i;
  char	*var_parsed;

  i = 0;
  while (var[i] != '\0' && var[i] != '=' && var)
    i++;
  if (var[i] == '=' && (i + 1) < my_strlen(var))
    {
      if ((var_parsed = malloc((my_strlen(var) - i) + 2)) == NULL)
        return (NULL);
      var = &var[i + 1];
      var_parsed = my_strcpy(var_parsed, var);
      return (var_parsed);
    }
  else
    return (NULL);
}

char	*find_path(char **env, char *wanted)
{
  int	i;
  int	stop;

  stop = 0;
  i = 0;
  while (env[i] != NULL && stop == 0)
    {
      if (my_strncmp(env[i], wanted, 4) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    return (env[i]);
  else
    return (NULL);
}

char	*recover_env_var(char **env, char *var)
{
  int	i;
  int	stop;
  char	*var_found;

  i = 0;
  stop = 0;
  while (env[i] != NULL && stop == 0)
    {
      if (my_strncmp(env[i], var, my_strlen(var)) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  if (stop == 1)
    {
      if ((var_found = malloc(my_strlen(env[i]) + 1)) == NULL)
        return (NULL);
      var_found = my_strcpy(var_found, env[i]);
      return (var_found);
    }
  else
    return (NULL);
}

int	find_pos_var_in_env(char ***env, char *wanted)
{
  int	stop;
  int	i;

  i = 0;
  stop = 0;
  while ((*env)[i] != NULL && stop == 0)
    {
      if (my_strncmp((*env)[i], wanted, 6) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  return (i);
}

int	check_if_the_var_exist(char ***env, char *wanted)
{
  int	stop;
  int	i;

  i = 0;
  stop = 0;
  while ((*env)[i] != NULL && stop == 0)
    {
      if (my_strncmp((*env)[i], wanted, 6) == 1)
        stop = 1;
      if (stop == 0)
        i++;
    }
  return (stop);
}
