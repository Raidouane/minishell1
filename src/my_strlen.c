/*
** my_strlen.c for my_strlen.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Wed Jan 20 15:24:07 2016 Raidouane EL MOUKHTARI
** Last update Wed Jan 20 15:25:19 2016 Raidouane EL MOUKHTARI
*/

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str && str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
