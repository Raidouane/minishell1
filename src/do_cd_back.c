/*
** do_cd_back.c for do_cd_back.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/las/PSU_2015_minishell1/src_normed
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Fri Jan 22 02:33:09 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 02:35:10 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	**fill_env_to_new_env(char ***env, char **new_env)
{
  int	i;

  i = 0;
  while (*env && (*env)[i] != NULL)
    {
      new_env[i] = (*env)[i];
      i++;
    }
  new_env[i] = NULL;
  return (new_env);
}

int	insert_oldpwd_to_env(char ***env)
{
  char	*old_pwd;
  int	size;
  char	**new_env;

  size = 0;
  while ((*env)[size] != NULL)
    size++;
  if ((new_env = malloc(sizeof(char **) * (size + 2))) == NULL)
    return (-1);
  new_env = fill_env_to_new_env(env, new_env);
  old_pwd = create_oldpwd_var(*env);
  if ((new_env[size] = malloc(my_strlen(old_pwd) + 1)) == NULL)
    return (-1);
  new_env[size] = my_strcpy(new_env[size], old_pwd);
  new_env[size + 1] = NULL;
  *env = new_env;
  return (1);
}

int	realloc_var_and_change(char ***env, int pos, char *cpy_pwd)
{
  int	i;
  int	u;
  char	*cpy;

  i = 0;
  if ((cpy = malloc((my_strlen(cpy_pwd) + my_strlen((*env)[pos])))) == NULL)
    return (-1);
  while ((*env)[pos][i] != '=' && (*env)[pos][i] != '\0' && (*env)[pos])
    i++;
  if ((*env)[pos][i] == '=')
    {
      cpy = my_strcpy(cpy, (*env)[pos]);
      u = i + 1;
      i = 0;
      while (cpy_pwd[i] != '\0' && cpy_pwd)
        cpy[u++] = cpy_pwd[i++];
      cpy[u] = '\0';
      if (((*env)[pos] = malloc(my_strlen(cpy) + 1)) == NULL)
        return (-1);
      (*env)[pos] = my_strcpy((*env)[pos], cpy);
      free(cpy);
    }
  else
    return (-1);
  return (0);
}

int	swap_var_in_env(char ***env, char *cpy_pwd, char *cpy_oldpwd)
{
  int	stop;
  int	pos;

  stop = check_if_the_var_exist(env, "OLDPWD");
  if (stop == 1)
    {
      pos = find_pos_var_in_env(env, "OLDPWD");
      if (realloc_var_and_change(env, pos, cpy_pwd) == -1)
	return (-1);
    }
  stop = check_if_the_var_exist(env, "PWD");
  if (stop == 1)
    {
      pos = find_pos_var_in_env(env, "PWD");
      if (realloc_var_and_change(env, pos, cpy_oldpwd) == -1)
	return (-1);
    }
  return (1);
}

int	do_back_cd(char ***env)
{
  char	*cpy_pwd;
  char	*cpy_oldpwd;
  int	pos;

  if (check_if_the_var_exist(env, "OLDPWD") == 1)
    {
      pos = find_pos_var_in_env(env, "OLDPWD");
      if ((cpy_oldpwd = malloc(my_strlen((*env)[pos]) + 1)) == NULL)
        return (-1);
      cpy_oldpwd = my_strcpy(cpy_oldpwd, (*env)[pos]);
      cpy_oldpwd = parse_var_env(cpy_oldpwd);
      if ((cpy_pwd = recover_env_var(*env, "PWD")) == NULL)
        return (-1);
      cpy_pwd = parse_var_env(cpy_pwd);
      if ((swap_var_in_env(env, cpy_pwd, cpy_oldpwd)) == -1)
        return (-1);
    }
  else
    {
      if (insert_oldpwd_to_env(env) == -1)
        return (-1);
      do_back_cd(env);
    }
  return (1);
}
