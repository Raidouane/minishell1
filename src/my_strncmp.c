/*
** my_strcmp.c for my_strncmp.c in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Tue Jan 12 23:18:44 2016 Raidouane EL MOUKHTARI
** Last update Tue Jan 19 00:01:36 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;
  int	m;
  int	check;

  m = 0;
  i = 0;
  check = 0;
  while (m <= n && s1[i] != '\0' && s2[i] != '\0')
    {
      if (s1[i] == s2[i])
	+check++;
      i++;
      m++;
    }
  if (check == my_strlen(s2) || check == my_strlen(s1))
    return (1);
  else
    return (0);
}
