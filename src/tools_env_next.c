/*
** tools_env_next.c for tools_env_next.c in /home/el-mou_r/rendu/PSU/PSU_2015_minishell1/src
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Sun Jan 24 03:05:00 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 03:06:03 2016 Raidouane EL MOUKHTARI
*/

#include "my.h"

char	*fill_var_found(char *str, char **env, int i)
{
  int	t;

  t = 0;
  while (env[i][t] != '=' && env[i][t] != '\0' && env[i][t])
    {
      str[t] = env[i][t];
      t++;
    }
  str[t] = '\0';
  return (str);
}

char	*search_var(char *wanted, char **env)
{
  int	i;
  int	t;
  char	*str;

  i = 0;
  while (env[i] != NULL && env)
    {
      t = 0;
      while (env[i][t] != '=' && env[i][t] != '\0' && env[i][t])
	t++;
      if ((str = malloc(t + 1)) == NULL)
	return (NULL);
      str = fill_var_found(str, env, i);
      if (my_strstr(str, wanted, my_strlen(wanted)) == 1)
	{
	  free(str);
	  return (env[i]);
	}
      else
	free(str);
      i++;
    }
  return (NULL);
}

char	*create_new_var(char **newargv, int nb_args)
{
  char	*new_var;

  if (nb_args == 1)
    {
      if ((new_var = malloc(my_strlen(newargv[1]) + 3)) == NULL)
        return (NULL);
      new_var = my_strcpy(new_var, newargv[1]);
      new_var = my_strcat(new_var, "=");
    }
  else if (nb_args == 2)
    {
      if ((new_var = malloc(my_strlen(newargv[1]) + 3 +
			    my_strlen(newargv[2]))) == NULL)
	return (NULL);
      new_var = my_strcpy(new_var, newargv[1]);
      new_var = my_strcat(new_var, "=");
      new_var = my_strcat(new_var, newargv[2]);
    }
  return (new_var);
}

int	realloc_a_var(char ***env, int i, char *path)
{
  int	size;
  char	*new;

  size = my_strlen((*env)[i]);
  if ((new = malloc(size + my_strlen(path) + 2)) == NULL)
    return (-1);
  new = my_strcpy(new, (*env)[i]);
  new = my_strcat(new, "/");
  new = my_strcat(new, path);
  (*env)[i] = new;
  return (0);
}
