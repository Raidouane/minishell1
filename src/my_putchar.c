/*
** my_putchar.c for my_putchar in /home/da-fon_s/rendu/Piscine_C_J06
** 
** Made by samuel da-fonseca
** Login   <da-fon_s@epitech.net>
** 
** Started on  Tue Oct  6 11:11:11 2015 samuel da-fonseca
** Last update Thu Jan 14 16:41:07 2016 Raidouane EL MOUKHTARI
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}
