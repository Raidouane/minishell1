/*
** my.h for my.h in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Thu Jan 14 16:36:19 2016 Raidouane EL MOUKHTARI
** Last update Sun Jan 24 02:49:32 2016 Raidouane EL MOUKHTARI
*/

#ifndef MY_H_
#define MY_H_

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include "get_next_line.h"

char	*my_strcat(char *, char *);
char	*my_strcpy(char *, char *);
void	my_putchar(char);
void	my_putstr(char *);
int	my_strstr(char *s1, char *s2, int n);
int	my_strlen(char *);
int	my_strncmp(char *, char *, int);
char	*gnl(char **save);
char	*gnl_else(int i, char *s, char **save, int t);
char	*get_next_line(const int fd);
char	**fill_newargv(char *s, char **read, char **newargv, int i);
char	*fill_newargv_next(char *save, char **read, int nb_space, int nb_tab);
char	*parse_my_cmd(char *s, char *already_read, int *nb_space, int *nb_tab);
char	*fill_str_parse_my_cmd(char *str, int i, char *s);
char	*parse_my_cmd_next(char *s, int i, int *nb_space, int *nb_tab);
char	**my_newargs(char *s, char *path, int nb_args, char **env);
char	*check_cmd(char *path_env, char *cmd, char *cpy_cmd, int jump);
char	*find_path(char **env, char *wanted);
int	parse_my_args(char *s, char *path, int nb_space);
char	*parse_my_path(char *s, int *space);
char	*parse_my_path_next(char *s, int i, int size);
char	*parse_uservar(char *uservar);
char	*path_home_coll(char *user);
char	*back_pwd(char *pwdvar);
char	*back_pwd_next(int i, char *pwdvar, int nb_slash);
int	change_pwdvar(char ***env, char *path);
int	do_cd(char **newargv, char ***env);
int	check_if_direcory_exist(DIR *directory, char **newargv, char ***env, int pass);
int	do_cd_basic(char ***env);
int	change_pwd_only_cd(char ***env);
char	*write_pwd(char *var);
int	do_back_cd(char ***env);
int	check_if_the_var_exist(char ***env, char *wanted);
int	find_pos_var_in_env(char ***env, char *wanted);
int	swap_var_in_env(char ***env, char *cpy_pwd, char *cpy_oldpwd);
int	realloc_var_and_change(char ***env, int pos, char *cpy_pwd);
char	**fill_env_to_new_env(char ***env, char **new_env);
int	insert_oldpwd_to_env(char ***env);
char	*recover_env_var(char **env, char *var);
char	*create_oldpwd_var(char **env);
int	realloc_a_var(char ***env, int i, char *path);
char	*create_new_var(char **newargv, int nb_args);
char	*parse_var_env(char *var);
char    *concaitain_read(char **read, char *save, char *str);
int	my_setenv(char ***env, char **newargv, int nb_args);
int	realloc_env_setenv(char ***env, char **newargv, int nb_args);
int	edit_var(char ***env, char **newargv, int nb_args);
int	my_unsetenv(char ***env, char **newargv, int nb_args);
int	realloc_env_unsetenv(char ***env, char *var_to_delete);
char	**fill_env_to_new_env_unsetenv(char ***env, char **new_env, char *var_to_delete);
int     main_loop(char ***env);
int     launch(char *s, char *path, int nb_space, char ***env);
int	free_newargv(char **newargv, char *path);
int	my_exec(char *program, char **newargv, char **env);
int	lauch_child_process(char *program, char **newargv, char **env);
void	sigintHandler();
int	check_path(char *path);
int     check_first_av(char *path);
char	*search_var(char *wanted, char **env);
char	*fill_var_found(char *str, char **env, int i);
int     parse_var_unsetenv(char *s1, char *s2);

#endif /* !MY_H_ */
