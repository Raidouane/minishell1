/*
** get_next_line.h for get_next_line.h in /home/el-mou_r/rendu/CPE/CPE_2015_getnextline
** 
** Made by Raidouane EL MOUKHTARI
** Login   <el-mou_r@epitech.net>
** 
** Started on  Mon Jan  4 11:14:39 2016 Raidouane EL MOUKHTARI
** Last update Mon Jan 18 23:07:41 2016 Raidouane EL MOUKHTARI
*/

# ifndef READ_SIZE
# define READ_SIZE (100000)
#endif /* !READ_SIZE */
