##
## Makefile for Makefile in /home/el-mou_r/rendu/bootstraps/PSU_2015_my_exec
## 
## Made by Raidouane EL MOUKHTARI
## Login   <el-mou_r@epitech.net>
## 
## Started on  Wed Jan 13 01:09:37 2016 Raidouane EL MOUKHTARI
## Last update Sun Jan 24 13:45:41 2016 Raidouane EL MOUKHTARI
##

CC	= gcc

RM	= rm

NAME	= mysh

CFLAGS	= -I./include -Werror -Wall -W -lncurses

SRC	=src/my_putchar.c	\
	src/my_putstr.c		\
	src/my_exec.c		\
	src/my_strlen.c		\
	src/my_strcat.c		\
	src/get_next_line.c	\
	src/my_strncmp.c	\
	src/main.c		\
	src/my_strstr.c		\
	src/my_strcpy.c		\
	src/cd_routes.c		\
	src/do_cd_back_next.c	\
	src/do_cd_previous.c  	\
	src/newargv.c 		\
	src/tools_env.c		\
	src/do_cd_back.c	\
	src/do_cd_basic.c	\
	src/fill_newargv.c	\
	src/parsing.c		\
	src/tools_env_next.c	\
	src/my_setenv.c		\
	src/my_unsetenv.c

OBJ	= $(SRC:.c=.o)

$(NAME): 	$(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(NAME)

all:	$(NAME)

clean:
	$(RM) -f $(OBJ)

fclean:	clean
	$(RM) -f $(NAME)

re:	fclean all
